#!/bin/sh

# (1) builds the project with production environment
# (2) generates a .zip file with the current timestamp to be uploaded to Moodle

FILENAME=mokolo_`date +%s`.zip

cd mokolo && \
grunt default --env=prod && \
cd - && \
zip -r ${FILENAME} mokolo -x *node_modules* && \
cp ${FILENAME} mokolo_latest.zip && \
cd mokolo && \
grunt default --env=dev && \
cd - && \
git commit -m "latest version packaged" mokolo_latest.zip