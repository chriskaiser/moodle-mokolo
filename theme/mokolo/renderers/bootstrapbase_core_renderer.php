<?php

require_once($CFG->dirroot . '/theme/bootstrapbase/renderers/core_renderer.php');
require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/weblib.php');
require_once($CFG->dirroot . '/course/renderer.php');

class theme_mokolo_core_renderer extends theme_bootstrapbase_core_renderer {

    public function page_colorscheme() {
        global $PAGE;

        $htmlclass = '';

        if ($PAGE->category != NULL && property_exists($PAGE->category, 'name')) {
            $htmlclass .= 'page-colorscheme page-colorscheme--' . strtolower($PAGE->category->name);
        }

        return $htmlclass;
    }

    public function page_sidebar_header($course, $modinfo) {
        global $PAGE;

        $output = '';

        $category = coursecat::get($PAGE->category->parent);

        $output .= html_writer::start_tag('div', array('class' => 'page_sidebar-header'));


        $backtocourses = array(
            'label' => get_string('backtocourses', 'theme_mokolo'),
            'link' => new moodle_url('/course/')
        );

        $output .= html_writer::link($backtocourses['link'], $backtocourses['label'], array('class' => 'page_sidebar-header-link'));

        $output .= html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $category->id)), $category->name, array('class' => 'page_sidebar-header-title'));

        $output .= html_writer::tag('div', $course->fullname,
            array('class' => 'page_sidebar-header-subtitle'));

        $output .= html_writer::end_tag('div');

        echo $output;
    }

    public function page_course_search_form () {
        global $PAGE;

        $courserenderer = $PAGE->get_renderer('core', 'course');

        echo $courserenderer->course_search_form('', 'navbar');
    }

    public function page_sidebar($type = 'course', $currentsectionid = NULL) {

        if ($type == 'course') {
            return $this->page_sidebar_course($currentsectionid);
        }

        if ($type == 'coursecategory') {
            return $this->page_sidebar_coursecategory();
        }
    }

    private function page_sidebar_coursecategory() {
        global $PAGE;

        $currentcatid = optional_param('categoryid', 0, PARAM_INT); // Category id

        $parentcat = coursecat::get(0);

        $subcategories = $parentcat->get_children();

        $content = '';

        $content .= html_writer::start_tag('ul', array('class' => 'page_sidebar-list'));

        foreach($subcategories as $cat) {
            $itemclasses = 'page_sidebar-list-item';
            $linktocat = '';

            $num_courses = $this->get_num_of_courses_of_cat($cat);

            if ($num_courses > 0) {
                $itemclasses .= ' page_sidebar-list-item--has-children';
                $linktocat = new moodle_url('/course/index.php', array('categoryid' => $cat->id));
            }

            if ($cat->id == $currentcatid || $currentcatid == 0) {
                $currentcatid = $cat->id;
                $itemclasses .= ' page_sidebar-list-item--is-active';
            }

            $cname = $categoryname = $cat->get_formatted_name();
            $content .= html_writer::start_tag('li', array('class' => $itemclasses));
            $content .= html_writer::link($linktocat, $cat->get_formatted_name() . ' (' . $num_courses . ')', array('class' => 'page_sidebar-list-item-link'));
            $content .= html_writer::end_tag('li');

        }
        
        $content .= html_writer::end_tag('ul');

        return $content;

    }

    private function get_num_of_courses_of_cat ($cat) {
        $num_courses = 0;

        $children = $cat->get_children();

        if (count($children) == 0) {
            $courses = $cat->get_courses();
            return count($courses);
        }

        foreach ($children as $subcat) {
            $num_courses += $this->get_num_of_courses_of_cat($subcat);
        }

        return $num_courses;
    }

    private function page_sidebar_course ($currentsectionid = NULL) {
        global $PAGE;

        $course = $PAGE->course;
        $modinfo = get_fast_modinfo($course);
        $completion = new completion_info($course);

        $this->page_sidebar_header($course, $modinfo);

        echo html_writer::start_tag('div', array('class' => 'topic_navigation'));

        $currentsection = NULL;
        $params = $PAGE->url->params();
        if (isset($params['section'])) {
            $currentsection = $params['section'];
        }

        foreach ($modinfo->get_sections() as $section => $cms) {
            if ($section == 0) {
                continue;
            }

            $thissection = $modinfo->get_section_info($section);

            if ($currentsection === NULL && $currentsectionid !== NULL) {
                if ($thissection->id === $currentsectionid) {
                    $currentsection = $section;
                }
            }


            $showsection = 
                $thissection->uservisible ||
                ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));

            if (!$showsection) {
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            $urltosection = new moodle_url('/course/view.php',
                array('id' => $course->id,
                      'section' => $section));

            $sectioncontent = '';

            // MOKOLO_COMPLETION
            // In default Moodle, a SECTION/TOPIC has NOT the ability to be complete
            // hence, we define here, that whenever ONE of the activities/resources
            // inside the SECTION/TOPIC is complete, then the whole thing is complete
            $section_is_complete = false;

            $sectiontype = NULL;

            foreach ($cms as $modnumber) {
                $cm = $modinfo->cms[$modnumber];
                $completion_cm = $completion->get_data($cm);

                if ($completion_cm->completionstate != COMPLETION_INCOMPLETE) {
                    $section_is_complete = true;
                }

                if ($sectiontype === NULL) {
                    switch ($cm->modname) {
                        case 'quiz':
                            $sectiontype = 'quiz';
                            break;
                        default: 
                            $sectiontype = 'video';
                            break;
                    }
                }
            }

            $navClasses = 'topic_navigation-item';

            if ($section_is_complete) {
                $navClasses .= ' topic_navigation-item--is-complete';
            }
            if ($section == $currentsection) {
                $navClasses .= ' topic_navigation-item--is-active';   
            }

            $sectioncontent .= html_writer::start_tag('div', array('class' => $navClasses));

            $islastelement = false;

            if ($islastelement) {
                $sectioncontent .= html_writer::tag('span', '',
                    array('class' => 'topic_navigation-item-topic_progress topic_navigation-item-topic_progress--is-last'));
            } else {
                $sectioncontent .= html_writer::tag('span', '',
                    array('class' => 'topic_navigation-item-topic_progress'));
            }

            $iconClasses = 'topic_navigation-item-topic_icon topic_navigation-topic_icon--is-' . $sectiontype;

            $sectioncontent .= html_writer::tag('i', '',
                array('class' => $iconClasses));

            $sectioncontent .= html_writer::tag('span', $thissection->name,
                array('class' => 'topic_navigation-item-topic_name'));

            // TODO: where to take this information from?
            // $sectioncontent .= html_writer::tag('span', 'film 2:35',
            //     array('class' => 'topic_navigation-item-topic_subtitle'));

            $sectioncontent .= html_writer::end_tag('div');

            echo html_writer::tag('a', $sectioncontent, array('href' => $urltosection, 'class' => 'topic_navigation-link'));
        }

        echo html_writer::end_tag('div');
    }


    public function page_statusbar() {
        global $PAGE;

        $output = '';

        $course = $PAGE->course;
        $modinfo = get_fast_modinfo($course);
        $completion = new completion_info($course);

        $output .= html_writer::tag('span', $course->fullname,
            array('class' => 'page_statusbar-title'));

        $output .= html_writer::start_tag('ul',
            array('class' => 'dotted_progress'));

        foreach ($modinfo->get_sections() as $section => $cms) {
            if ($section == 0) {
                continue;
            }

            $thissection = $modinfo->get_section_info($section);

            $showsection = 
                $thissection->uservisible ||
                ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));

            if (!$showsection) {
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            // MOKOLO_COMPLETION
            // In default Moodle, a SECTION/TOPIC has NOT the ability to be complete
            // hence, we define here, that whenever ONE of the activities/resources
            // inside the SECTION/TOPIC is complete, then the whole thing is complete
            $section_is_complete = false;

            foreach ($cms as $modnumber) {
                $cm = $modinfo->cms[$modnumber];
                $completion_cm = $completion->get_data($cm);

                if ($completion_cm->completionstate != COMPLETION_INCOMPLETE) {
                    $section_is_complete = true;
                }
            }

            if ($section_is_complete) {
                $output .= html_writer::tag('li', '',
                    array('class' => 'dotted_progress-item dotted_progress-item--is-achieved'));
            } else {
                $output .= html_writer::tag('li', '',
                    array('class' => 'dotted_progress-item'));
            }
        }

        $output .= html_writer::end_tag('ul');
    
        echo $output;        
    }

}
