<?php

require_once($CFG->dirroot . '/course/format/topics/renderer.php');
require_once($CFG->dirroot . '/lib/medialib.php');
require_once($CFG->dirroot . '/lib/datalib.php');
require_once($CFG->dirroot . '/lib/weblib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/lib/outputrenderers.php');
require_once($CFG->dirroot . '/mod/resource/lib.php');
require_once($CFG->dirroot . '/mod/resource/locallib.php');
require_once($CFG->dirroot . '/mod/choice/lib.php');
require_once($CFG->dirroot . '/mod/forum/lib.php');
require_once($CFG->dirroot . '/mod/url/locallib.php');
require_once($CFG->libdir  . '/gradelib.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once($CFG->libdir  . '/completionlib.php');
require_once($CFG->dirroot . '/course/format/lib.php');
require_once($CFG->dirroot . '/lib/weblib.php');

class theme_mokolo_format_topics_renderer extends format_topics_renderer {

    /**
     * This is just to redirect, if a user is (mistakenly) redirected to the course page, instead of the
     * section page.
     */
    public function print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        // check if the user is administrator, if so, just use the parent's renderer
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
        if (has_capability('moodle/user:loginas', $coursecontext)) {
            return parent::print_multiple_section_page($course, $sections, $mods, $modnames, $modnamesused);
        }

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) {
                continue;
            }

            // Can we view the section in question?
            if (!($sectioninfo = $modinfo->get_section_info($section))) {
                // This section doesn't exist
                print_error('unknowncoursesection', 'error', null, $course->fullname);
                return;
            }

            if (!$sectioninfo->uservisible) {
                if (!$course->hiddensections) {
                    echo $this->start_section_list();
                    echo $this->section_hidden($displaysection, $course->id);
                    echo $this->end_section_list();
                }
                // Can't view this section.
                continue;
            }

            $urltosection = new moodle_url('/course/view.php',
                array('id' => $course->id,
                      'section' => $section));

            redirect($urltosection);
            return;
        }
    }

    public function print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        // check if the user is administrator, if so, just use the parent's renderer
        $coursecontext = get_context_instance(CONTEXT_COURSE, $course->id);
        if (has_capability('moodle/user:loginas', $coursecontext)) {
            return parent::print_single_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection);
        }

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        // Can we view the section in question?
        if (!($sectioninfo = $modinfo->get_section_info($displaysection))) {
            // This section doesn't exist
            print_error('unknowncoursesection', 'error', null, $course->fullname);
            return;
        }

        if (!$sectioninfo->uservisible) {
            if (!$course->hiddensections) {
                echo $this->start_section_list();
                echo $this->section_hidden($displaysection, $course->id);
                echo $this->end_section_list();
            }
            // Can't view this section.
            return;
        }

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, $displaysection);
        $thissection = $modinfo->get_section_info(0);
        if ($thissection->summary or !empty($modinfo->sections[0]) or $PAGE->user_is_editing()) {
            echo $this->start_section_list();
            echo $this->section_header($thissection, $course, true, $displaysection);
            echo $this->courserenderer->course_section_cm_list($course, $thissection, $displaysection);
            echo $this->courserenderer->course_section_add_cm_control($course, 0, $displaysection);
            echo $this->section_footer();
            echo $this->end_section_list();
        }

        // The requested section page.
        $thissection = $modinfo->get_section_info($displaysection);

        // Now the content of the section...

        $modinfo = get_fast_modinfo($course);
        if (is_object($thissection)) {
            $thissection = $modinfo->get_section_info($thissection->section);
        } else {
            $thissection = $modinfo->get_section_info($section);
        }

        $videomod_yt = null;
        $videomod_alt = null;
        $forummod = null;
        $quizmod = null;
        $downloadmods = array();

        if (!array_key_exists($thissection->section, $modinfo->sections)) {
            echo html_writer::start_tag('section', array('class' => 'page_resourcecontent'));
            echo "WARNING: No resources in section. Please check with your administrator.";
            echo html_writer::end_tag('section');
            return;
        }

        foreach ($modinfo->sections[$thissection->section] as $modnumber) {
            $mod = $modinfo->cms[$modnumber];

            switch ($mod->modname) {
                case 'url':
                    $videomod_yt = $mod;
                    break;
                case 'quiz':
                    $quizmod = $mod;
                    break;
                case 'forum':
                    $forummod = $mod;
                    break;
                case 'resource':
                    array_push($downloadmods, $mod);
                    break;
                default: 
                    //echo "unknown mod type: " . $mod->modname;
                    break;
            }
        }

        $resourcecontent_modifier = '';
        if (count($downloadmods) == 0) {
            $resourcecontent_modifier = ' page_resourcecontent--no-left-column';
        }

        echo html_writer::start_tag('section', array('class' => 'page_resourcecontent' . $resourcecontent_modifier));

        if ($videomod_yt == null) {
            // try to see if there is a video file and use this
            $videomod_alt = $this->find_video_file($downloadmods, $course);

            if ($videomod_alt == null) {

                if ($quizmod == null) {
                    echo "WARNING: No video resource or quiz in section. Please check with your administrator.";
                    echo html_writer::end_tag('section');
                    return;
                }
            }
        }

        if ($forummod == null && $quizmod == null) {
            echo "WARNING: No forum resource in section. Please check with your administrator.";
            echo html_writer::end_tag('section');
            return;
        }

        // there can be either a quiz, a video or a youtube video being rendered
        if ($quizmod !== null) {
            $this->render_quiz($quizmod, $course);
        }
        else if ($videomod_yt !== null) {
            $this->render_video_yt($videomod_yt, $course);
        }
        else if ($videomod_alt !== null) {
            $this->render_video_alt($videomod_alt, $course);
        }

        if ($forummod !== null) {
            $this->render_forum($forummod, $course);
        }

        echo html_writer::end_tag('section');


        if (count($downloadmods) > 0) {

            echo html_writer::start_tag('section', array('class' => 'page_secondarycontent'));
            echo html_writer::start_tag('div', array('class' => 'page_secondarycontent-container'));

            if (count($downloadmods) > 0) {
                $this->render_downloads($downloadmods, $course);
            }

            // Close single-section div.
            echo html_writer::end_tag('div');
            echo html_writer::end_tag('section');
        }
    }

    private function render_video_yt ($mod, $course) {
        global $PAGE, $DB;

        $url = $DB->get_record('url', array('id'=>$mod->instance), '*', MUST_EXIST);

        $fullurl  = url_get_full_url($url, $mod, $course);
        $moodleurl = new moodle_url($fullurl);
        $title    = $url->name;
        $embedoptions = array(
            core_media::OPTION_TRUSTED => true,
            core_media::OPTION_BLOCK => true
        );

        $mediarenderer = $PAGE->get_renderer('core', 'media');
        echo $mediarenderer->embed_url($moodleurl, $title, '100%', 574, $embedoptions);
    }

    private function render_video_alt ($mod, $course) {
        global $PAGE, $DB, $CFG;

        $resource = $DB->get_record('resource', array('id'=>$mod->instance), '*', MUST_EXIST);

        $context = context_module::instance($mod->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
        if (count($files) < 1) {
            resource_print_filenotfound($resource, $mod, $course);
            die;
        } else {
            $file = reset($files);
            unset($files);
        }

        $context = context_module::instance($mod->id);
        $path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
        $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
        $moodleurl = new moodle_url('/pluginfile.php' . $path);

        $title    = $resource->name;
        $embedoptions = array(
            core_media::OPTION_TRUSTED => true,
            core_media::OPTION_BLOCK => true
        );

        $mediarenderer = $PAGE->get_renderer('core', 'media');
        echo $mediarenderer->embed_url($moodleurl, $title, '100%', 574, $embedoptions);
    }

    private function render_forum ($mod, $course) {
        global $DB;

        $forum = $DB->get_record("forum", array("id" => $mod->instance));

        echo html_writer::start_tag('div', array('class' => 'community'));

        echo html_writer::start_tag('div', array('class' => 'community-toolbar'));
        echo html_writer::start_tag('span', array('class' => 'community-toolbar-headlines'));

        echo html_writer::start_tag('span', array('class' => 'community-toolbar-headline community-toolbar-headline--is-active'));
        echo $mod->name;
        echo html_writer::end_tag('span');

        echo html_writer::end_tag('span');

        // echo html_writer::start_tag('span', array('class' => 'community-toolbar-filters'));
        // echo html_writer::start_tag('span', array('class' => 'community-toolbar-filter community-toolbar-filter--is-active'));
        // echo 'Top'; //TODO: translations
        // echo html_writer::end_tag('span');
        // echo html_writer::start_tag('span', array('class' => 'community-toolbar-filter'));
        // echo 'Recent'; //TODO: translations
        // echo html_writer::end_tag('span');
        // echo html_writer::end_tag('span');

        echo html_writer::end_tag('div');

        echo html_writer::start_tag('div', array('class' => 'community-content'));

        echo forum_print_latest_discussions($course, $forum);

        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');
    }

    private function render_quiz ($cm, $course) {
        global $OUTPUT, $USER, $PAGE;

        $context = context_module::instance($cm->id);

        // Create an object to manage all the other (non-roles) access rules.
        $timenow = time();
        $quizobj = quiz::create($cm->instance, $USER->id);
        $accessmanager = new quiz_access_manager($quizobj, $timenow,
                has_capability('mod/quiz:ignoretimelimits', $context, null, false));
        $quiz = $quizobj->get_quiz();

        $canattempt = has_capability('mod/quiz:attempt', $context);
        $canreviewmine = has_capability('mod/quiz:reviewmyattempts', $context);
        $canpreview = has_capability('mod/quiz:preview', $context);

        // Create view object which collects all the information the renderer will need.
        $viewobj = new mod_quiz_view_object();
        $viewobj->accessmanager = $accessmanager;
        $viewobj->canreviewmine = $canreviewmine;

        // Get this user's attempts.
        $attempts = quiz_get_user_attempts($quiz->id, $USER->id, 'finished', true);
        $lastfinishedattempt = end($attempts);
        $unfinished = false;
        if ($unfinishedattempt = quiz_get_user_attempt_unfinished($quiz->id, $USER->id)) {
            $attempts[] = $unfinishedattempt;

            // If the attempt is now overdue, deal with that - and pass isonline = false.
            // We want the student notified in this case.
            $quizobj->create_attempt_object($unfinishedattempt)->handle_if_time_expired(time(), false);

            $unfinished = $unfinishedattempt->state == quiz_attempt::IN_PROGRESS ||
                    $unfinishedattempt->state == quiz_attempt::OVERDUE;
            if (!$unfinished) {
                $lastfinishedattempt = $unfinishedattempt;
            }
            $unfinishedattempt = null; // To make it clear we do not use this again.
        }
        $numattempts = count($attempts);

        $viewobj->attempts = $attempts;
        $viewobj->attemptobjs = array();
        foreach ($attempts as $attempt) {
            $viewobj->attemptobjs[] = new quiz_attempt($attempt, $quiz, $cm, $course, false);
        }

        // Work out the final grade, checking whether it was overridden in the gradebook.
        if (!$canpreview) {
            $mygrade = quiz_get_best_grade($quiz, $USER->id);
        } else if ($lastfinishedattempt) {
            // Users who can preview the quiz don't get a proper grade, so work out a
            // plausible value to display instead, so the page looks right.
            $mygrade = quiz_rescale_grade($lastfinishedattempt->sumgrades, $quiz, false);
        } else {
            $mygrade = null;
        }

        $mygradeoverridden = false;
        $gradebookfeedback = '';

        $grading_info = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $USER->id);
        if (!empty($grading_info->items)) {
            $item = $grading_info->items[0];
            if (isset($item->grades[$USER->id])) {
                $grade = $item->grades[$USER->id];

                if ($grade->overridden) {
                    $mygrade = $grade->grade + 0; // Convert to number.
                    $mygradeoverridden = true;
                }
                if (!empty($grade->str_feedback)) {
                    $gradebookfeedback = $grade->str_feedback;
                }
            }
        }

        //$title = $course->shortname . ': ' . format_string($quiz->name);
        //$PAGE->set_title($title);
        //$PAGE->set_heading($course->fullname);
        $output = $PAGE->get_renderer('mod_quiz');

        // Print table with existing attempts.
        if ($attempts) {
            // Work out which columns we need, taking account what data is available in each attempt.
            list($someoptions, $alloptions) = quiz_get_combined_reviewoptions($quiz, $attempts);

            $viewobj->attemptcolumn  = $quiz->attempts != 1;

            $viewobj->gradecolumn    = $someoptions->marks >= question_display_options::MARK_AND_MAX &&
                    quiz_has_grades($quiz);
            $viewobj->markcolumn     = $viewobj->gradecolumn && ($quiz->grade != $quiz->sumgrades);
            $viewobj->overallstats   = $lastfinishedattempt && $alloptions->marks >= question_display_options::MARK_AND_MAX;

            $viewobj->feedbackcolumn = quiz_has_feedback($quiz) && $alloptions->overallfeedback;
        }

        $viewobj->timenow = $timenow;
        $viewobj->numattempts = $numattempts;
        $viewobj->mygrade = $mygrade;
        $viewobj->moreattempts = $unfinished ||
                !$accessmanager->is_finished($numattempts, $lastfinishedattempt);
        $viewobj->mygradeoverridden = $mygradeoverridden;
        $viewobj->gradebookfeedback = $gradebookfeedback;
        $viewobj->lastfinishedattempt = $lastfinishedattempt;
        $viewobj->canedit = has_capability('mod/quiz:manage', $context);
        $viewobj->editurl = new moodle_url('/mod/quiz/edit.php', array('cmid' => $cm->id));
        $viewobj->backtocourseurl = new moodle_url('/course/view.php', array('id' => $course->id));
        $viewobj->startattempturl = $quizobj->start_attempt_url();
        $viewobj->startattemptwarning = $quizobj->confirm_start_attempt_message($unfinished);
        $viewobj->popuprequired = $accessmanager->attempt_must_be_in_popup();
        $viewobj->popupoptions = $accessmanager->get_popup_options();

        // Display information about this quiz.
        $viewobj->infomessages = $viewobj->accessmanager->describe_rules();
        if ($quiz->attempts != 1) {
            $viewobj->infomessages[] = get_string('gradingmethod', 'quiz',
                    quiz_get_grading_option_name($quiz->grademethod));
        }

        // Determine wheter a start attempt button should be displayed.
        $viewobj->quizhasquestions = $quizobj->has_questions();
        $viewobj->preventmessages = array();
        if (!$viewobj->quizhasquestions) {
            $viewobj->buttontext = '';

        } else {
            if ($unfinished) {
                if ($canattempt) {
                    $viewobj->buttontext = get_string('continueattemptquiz', 'quiz');
                } else if ($canpreview) {
                    $viewobj->buttontext = get_string('continuepreview', 'quiz');
                }

            } else {
                if ($canattempt) {
                    $viewobj->preventmessages = $viewobj->accessmanager->prevent_new_attempt(
                            $viewobj->numattempts, $viewobj->lastfinishedattempt);
                    if ($viewobj->preventmessages) {
                        $viewobj->buttontext = '';
                    } else if ($viewobj->numattempts == 0) {
                        $viewobj->buttontext = get_string('attemptquiznow', 'quiz');
                    } else {
                        $viewobj->buttontext = get_string('reattemptquiz', 'quiz');
                    }

                } else if ($canpreview) {
                    $viewobj->buttontext = get_string('previewquiznow', 'quiz');
                }
            }

            // If, so far, we think a button should be printed, so check if they will be
            // allowed to access it.
            if ($viewobj->buttontext) {
                if (!$viewobj->moreattempts) {
                    $viewobj->buttontext = '';
                } else if ($canattempt
                        && $viewobj->preventmessages = $viewobj->accessmanager->prevent_access()) {
                    $viewobj->buttontext = '';
                }
            }
        }

        $viewobj->showbacktocourse = ($viewobj->buttontext === '' &&
                course_get_format($course)->has_view_page());

        if (isguestuser()) {
            // Guests can't do a quiz, so offer them a choice of logging in or going back.
            echo $output->view_page_guest($course, $quiz, $cm, $context, $viewobj->infomessages);
        } else if (!isguestuser() && !($canattempt || $canpreview
                  || $viewobj->canreviewmine)) {
            // If they are not enrolled in this course in a good enough role, tell them to enrol.
            echo $output->view_page_notenrolled($course, $quiz, $cm, $context, $viewobj->infomessages);
        } else {
            echo $output->view_page($course, $quiz, $cm, $context, $viewobj);
        }
    }

    private function find_video_file($mods, $course) {
        global $PAGE, $DB;

        $videomod = null;

        foreach ($mods as $mod) {
            if (!$mod->visible) {
                continue;
            }

            $file_type = $this->get_file_type($mod);

            if (stripos($file_type, 'video') !== false) {
                $videomod = $mod;
                break;
            }
        }

        return $videomod;
    }

    private function render_downloads ($mods, $course) {
        global $PAGE, $DB;

        $renderer = $PAGE->get_renderer('core','course');

        echo html_writer::start_tag('div', array('class' => 'downloads'));
        echo html_writer::start_tag('div', array('class' => 'downloads-headline'));
        echo get_string('download', 'repository');
        echo html_writer::end_tag('div');


        echo html_writer::start_tag('div', array('class' => 'downloads-content clearfix'));
        foreach ($mods as $mod) {
            if (!$mod->visible) {
                continue;
            }

            $linkToFile = $mod->url;

            $resource = $DB->get_record('resource', array('id'=>$mod->instance), '*', MUST_EXIST);

            echo html_writer::start_tag('a', array('class' => 'downloads-content-item clearfix', 'href' => $linkToFile, 'download' => 'download'));

            echo html_writer::start_tag('div', array('class' => 'downloads-content-item-image'));

            $file_type = $this->get_file_type($mod);

            if (stripos($file_type, 'audio') !== false) {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-audio-o'));
            } elseif (stripos($file_type, 'pdf') !== false) {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-pdf-o'));
            } elseif (stripos($file_type, 'video') !== false) {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-video-o'));
            } elseif (stripos($file_type, 'zip') !== false) {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-archive-o'));
            } elseif (stripos($file_type, 'text') !== false) {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-text-o'));
            } else {
                echo html_writer::tag('i', '', array('class' => 'fa fa-file-o'));
            }
            echo html_writer::end_tag('div');

            echo html_writer::start_tag('div', array('class' => 'downloads-content-item-metadata'));

            echo html_writer::tag('div', $resource->name, array('class' => 'downloads-content-item-title'));
            echo html_writer::tag('div', $this->get_file_type($mod), array('class' => 'downloads-content-item-type'));
            echo html_writer::tag('div', $this->get_file_size($mod), array('class' => 'downloads-content-item-size'));
            echo html_writer::tag('div', $this->get_file_date($mod), array('class' => 'downloads-content-item-modified'));

            $intro = str_replace("&nbsp;", " ", strip_tags($resource->intro));

            echo html_writer::tag('div', $intro, array('class' => 'downloads-content-item-intro'));
            echo html_writer::end_tag('div');

            echo html_writer::end_tag('a');
        }

        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');
    }

    private function get_file_size ($mod) {
        $resource = (object)array('displayoptions' => @serialize(array('showsize' => 1)));

        return resource_get_optional_details($resource, $mod);
    }

    private function get_file_date ($mod) {
        $resource = (object)array('displayoptions' => @serialize(array('showdate' => 1)));

        return resource_get_optional_details($resource, $mod);
    }

    private function get_file_type ($mod) {
        $resource = (object)array('displayoptions' => @serialize(array('showtype' => 1)));

        return resource_get_optional_details($resource, $mod);
    }
}
