<?php

require_once($CFG->dirroot . '/lib/coursecatlib.php');
require_once($CFG->dirroot . '/lib/weblib.php');
require_once($CFG->dirroot . '/lib/modinfolib.php');
require_once($CFG->dirroot . '/course/renderer.php');

class theme_mokolo_core_course_renderer extends core_course_renderer {

    public function course_category($category) {
        global $PAGE, $DB;

        $currentcat = coursecat::get(is_object($category) ? $category->id : $category);

        $currentcatid = $currentcat->id;

        if (can_edit_in_category($currentcatid)) {
            return parent::course_category($category);
        }

        if ($currentcatid == 0) {
            $currentcat = reset($currentcat->get_children());
            $currentcatid = $currentcat->id;
        }

        if (can_edit_in_category($currentcatid)) {
            return parent::course_category($category);
        }


        $content = '';

        $content .= html_writer::start_tag('div', array('class' => 'courses_overview-content_wrapper'));
        $content .= html_writer::start_tag('header', array('class' => 'courses_overview-headline'));
        $content .= html_writer::tag('span', $currentcat->get_formatted_name(), array('class' => 'courses_overview-headline_text'));
        $content .= html_writer::end_tag('header');

        $subcategories = $currentcat->get_children();

        $count_subcats = count($subcategories);

        $column_count_modifier = ' courses_overview-content_wrapper--' . $count_subcats;
        $content .= html_writer::start_tag('div', array('class' => 'courses_overview-content_wrapper' . $column_count_modifier));

        $catnum = 0;
        foreach($subcategories as $cat) {
            $itemclasses = 'courses_overview_container';

            $itemclasses .= ' courses_overview_container--' . strtolower($cat->get_formatted_name());

            if ($count_subcats == 4 && $catnum >= 2) {
                $itemclasses .= ' courses_overview_container--half';
            } else {
                $content .= html_writer::start_tag('div', array('class' => 'courses_overview-content_column'));
            }

            $content .= html_writer::start_tag('div', array('class' => $itemclasses));

            $content .= html_writer::tag('header', $cat->get_formatted_name(), array('class' => 'courses_overview-title'));

            $courses = $cat->get_courses();

            foreach($courses as $course) {
                $cm = get_fast_modinfo($course->id);
                $coursesections = $cm->get_section_info_all();

                if (count($coursesections) > 1) {

                    $sectionid = 0;
                    foreach($coursesections as $courseindex => $coursesection) {
                        if ($coursesection->name != NULL) {
                            $sectionid = $courseindex;
                            break;
                        }
                    }

                    $linktocourse = new moodle_url('/course/view.php', array('id' => $course->id, 'section' => $sectionid));

                    $content .= html_writer::link($linktocourse, $course->shortname, array('class' => 'course_link'));
                } else {
                    $content .= html_writer::tag('div', $course->shortname, array('class' => 'course_link course_link--disabled'));
                }
            }

            if ($catnum < 2) {
                $content .= html_writer::end_tag('div');
            }
            $content .= html_writer::end_tag('div');

            $catnum++;
        }
        
        $content .= html_writer::end_tag('div');
        $content .= html_writer::end_tag('div');

        return $content;

    }
}
