<?php

$THEME->name = 'mokolo';

$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase');
$THEME->sheets = array('mokolo');
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->enable_dock = true;
$THEME->editor_sheets = array();


$THEME->layouts = array(
    // Standard layout with blocks, this is recommended for most pages with general information.
    'standard' => array(
        'file' => 'standard_3.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    // Main course page.
    'course' => array(
        'file' => 'course.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu' => true),
    ),
    'coursecategory' => array(
        'file' => 'coursecategory.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    // Part of course, typical for modules - default page layout if $cm specified in require_login().
    'incourse' => array(
        'file' => 'course.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    // The site home page.
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('nonavbar' => true),
    ),
    // My dashboard page.
    'mydashboard' => array(
        'file' => 'dashboard.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu' => true),
    ),
    // My public page.
    'mypublic' => array(
        'file' => 'standard_2.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // Login page.
    'login' => array(
        'file' => 'login.php',
        'regions' => array(),
        'options' => array('langmenu' => true),
    ),
    // My progress page.
    'myprogress' => array(
        'file' => 'progress.php',
        'regions' => array(),
        'defaultregion' => '',
        'options' => array('langmenu' => true),
    ),
);

$THEME->csspostprocess = 'theme_mokolo_process_css';
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->javascripts = array(
);
$THEME->javascripts_footer = array(
    //'libs',
    'script'
);