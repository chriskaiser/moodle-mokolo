<?php    
// Get the HTML for the settings bits.
$html = theme_mokolo_get_html_for_settings($OUTPUT, $PAGE);
?>

<footer class="page_footer">
    <div class="page-buttons">
        <?php echo $PAGE->button; ?>
    </div>
    <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    <p class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></p>
    <?php
    echo $html->footnote;
    #echo $OUTPUT->login_info();
    if ($pagetype != 'dashboard') {
    	echo $OUTPUT->standard_footer_html();
    }
    ?>
</footer>