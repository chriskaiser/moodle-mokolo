<?php


// use default theme if ADMIN
 if (is_siteadmin()) {
    include ($CFG->dirroot . '/theme/bootstrapbase/layout/columns3.php');
    return;
}

// do not show landing page, if already logged in
if (isloggedin()) {
    $redirecturl = NULL;

    if ($pagetype == 'front') {
        $redirecturl = new moodle_url('/my');
    } else if ($pagetype == 'login') {
        $redirecturl = new moodle_url('/login/logout.php');
    }
    
    if ($redirecturl != NULL) {
        redirect($redirecturl);
        return;
    }
}

$bodyclasses = array('mokolo', 'page', 'page--' . $pagetype, $OUTPUT->page_colorscheme());

// Get the HTML for the settings bits.
$html = theme_mokolo_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes($bodyclasses); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>
