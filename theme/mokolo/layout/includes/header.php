<header
    role="banner"
    class="page_header <?php echo $html->navbarclass ?> moodle-has-zindex">
    <a
        class="page_header-logo-container"  
        href="<?php echo $CFG->wwwroot;?>">
        <img
            class="page_header-logo"
            alt="<?php echo $SITE->shortname ?>"
            src="<?php echo $OUTPUT->pix_url('logo_oer_white-01', 'theme'); ?>" />
    </a>

    <?php if ($pagetype != 'front') { ?>
    <ul class="page_header-nav">
        <li class="item <?php if ($pagetype == 'coursesoverview') { echo "is-active"; } ?>">
            <a
                class="link"
                href="<?php echo $html->headernav['subjects']['link'] ?>">
                <?php echo $html->headernav['subjects']['label'] ?>
            </a>
        </li>
        <li class="item <?php if ($pagetype == 'myprogress') { echo "is-active"; } ?>">
            <a
                class="link"
                href="<?php echo $html->headernav['myprogress']['link'] ?>">
                <?php echo $html->headernav['myprogress']['label'] ?>
            </a>
        </li>
        <li class="item is-last <?php if ($pagetype == 'dashboard') { echo "is-active"; } ?>">
            <a
                class="link"
                href="<?php echo $html->headernav['home']['link'] ?>">
                <?php echo $html->headernav['home']['label'] ?>
            </a>
        </li>
    </ul>
    <?php } ?>

    <div
        class="page-header_search">
        <i class="fa fa-search"></i>
        <?php $OUTPUT->page_course_search_form() ?>
    </div>

    <?php echo $OUTPUT->navbar_button(); ?>
    <?php echo $OUTPUT->user_menu(); ?>
    <div class="nav-collapse collapse">
        <?php echo $OUTPUT->custom_menu(); ?>
        <ul class="nav pull-right">
            <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
        </ul>
    </div>
</header>