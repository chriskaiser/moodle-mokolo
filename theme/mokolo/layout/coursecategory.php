<?php

// setup of page
$pagetype = 'coursesoverview';

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
if (is_siteadmin()) {
    return;
}
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div
            class="page_container">
            <nav
                class="page_sidebar">
                <?php
                    echo $OUTPUT->page_sidebar('coursecategory');
                ?>
            </nav>
            <div class="page_content">
                <div class="courses_overview-content">
                <?php
                    echo $OUTPUT->main_content();
                ?>
                </div>
            </div>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>