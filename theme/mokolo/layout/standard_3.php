<?php

// setup of page
$pagetype = 'standard';


// Set default (LTR) layout mark-up for a three column page.
$regionmainbox = 'span9';
$regionmain = 'span8 pull-right';
$sidepre = 'span4 desktop-first-column';
$sidepost = 'span3 pull-right';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmainbox = 'span9 pull-right';
    $regionmain = 'span8';
    $sidepre = 'span4 pull-right';
    $sidepost = 'span3 desktop-first-column';
}

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div class="page_container">
            <div class="page_content page_content--no-sidebar">
                <?php echo $OUTPUT->full_header(); ?>

                <div id="page-content" class="row-fluid">
                    <div id="region-main-box" class="<?php echo $regionmainbox; ?>">
                        <div class="row-fluid">
                            <section id="region-main" class="<?php echo $regionmain; ?>">
                                <?php
                                echo $OUTPUT->course_content_header();
                                echo $OUTPUT->main_content();
                                echo $OUTPUT->course_content_footer();
                                ?>
                            </section>
                            <?php echo $OUTPUT->blocks('side-pre', $sidepre); ?>
                        </div>
                    </div>
                    <?php echo $OUTPUT->blocks('side-post', $sidepost); ?>
                </div>
            </div>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>