<?php

// setup of page
$pagetype = 'course';

$currentsectionid = NULL;

if (isset($PAGE->cm->section)) {
    $currentsectionid = $PAGE->cm->section;
}

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
if (is_siteadmin()) {
    return;
}
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div class="page_container">
            <nav
                class="page_sidebar">
                <?php
                    echo $OUTPUT->page_sidebar('course', $currentsectionid);
                ?>
            </nav>
            <div class="page_content">
                <section
                    class="page_statusbar">
                <?php
                    echo $OUTPUT->page_statusbar();
                ?>
                </section>

                <div
                    class="page_maincontent">
                    <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>