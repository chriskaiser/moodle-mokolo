<?php

// setup of page
$pagetype = 'login';

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page" class="container-fluid">

        <div id="page-content" class="row-fluid">
            <section id="region-main" class="span12">
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
            </section>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>