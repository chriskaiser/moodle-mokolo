<?php

// setup of page
$pagetype = 'myprogress';

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div class="progress-content">
        <?php
            echo $OUTPUT->main_content();
        ?>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>