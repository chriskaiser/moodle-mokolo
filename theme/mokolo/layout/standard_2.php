<?php

// setup of page
$pagetype = 'standard';


// Set default (LTR) layout mark-up for a two column page (side-pre-only).
$regionmain = 'span9 pull-right';
$sidepre = 'span3 desktop-first-column';
// Reset layout mark-up for RTL languages.
if (right_to_left()) {
    $regionmain = 'span9';
    $sidepre = 'span3 pull-right';
}

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div class="page_container">
            <div class="page_content page_content--no-sidebar">
                <?php echo $OUTPUT->full_header(); ?>
                <div id="page-content" class="row-fluid">
                    <section id="region-main" class="<?php echo $regionmain; ?>">
                        <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                        ?>
                    </section>
                    <?php echo $OUTPUT->blocks('side-pre', $sidepre); ?>
                </div>
            </div>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>