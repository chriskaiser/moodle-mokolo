<?php

// setup of page
$pagetype = 'dashboard';

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
if (is_siteadmin()) {
    return;
}
require_once(dirname(__FILE__) . '/includes/header.php');
?>

    <div id="page">
        <div
            id="page-content"
            class="page_container">
            <div
                class="column-wrapper">
                <section class="column column--left">
                    <?php
                        echo $OUTPUT->blocks('side-pre', '');
                    ?>
                </section>
                <section class="column column--middle">
                    <?php
                        echo $OUTPUT->course_content_header();
                        echo $OUTPUT->main_content();
                        echo $OUTPUT->course_content_footer();
                    ?>
                </section>
                <section class="column column--right">
                    <?php
                        echo $OUTPUT->blocks('side-post', '');
                    ?>
                </section>
            </div>
        </div>
    </div>

<?php
require_once(dirname(__FILE__) . '/includes/footer.php');
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>