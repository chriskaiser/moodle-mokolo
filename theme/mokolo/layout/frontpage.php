<?php

// setup of page
$pagetype = 'front';

require_once(dirname(__FILE__) . '/includes/start_of_html.php');
if (is_siteadmin()) {
    return;
}
require_once(dirname(__FILE__) . '/includes/header.php');

?>

    <div id="page">

        <div class="page_container">
            <section class="is--first">
                <div class="teaser">
                    <img 
                        class="teaser_logo"
                        width="600"
                        alt="<?php echo $SITE->shortname ?>"
                        src="<?php echo $OUTPUT->pix_url('logo_oer_white-01', 'theme'); ?>"
                    />
                    <?php echo $PAGE->course->summary ?>
                </div>
                <div class="login-area">
                    <div class="login-area_header">
                        log in
                        <img 
                            class="login-area_header_logo"
                            height="40"
                            alt="<?php echo $SITE->shortname ?>"
                            src="<?php echo $OUTPUT->pix_url('logo_oer-01', 'theme'); ?>"
                        />
                    </div>
                    <div class="login-area_content">
                        <a  
                            href="<?php echo get_login_url(); ?>"
                            class="btn btn-block btn-primary">
                            log in
                        </a>
                        <span class="or-separator">
                        or
                        </span>
                        <a  
                            href="<?php echo $CFG->wwwroot . '/login/signup.php'; ?>"
                            class="btn btn-block">
                            sign up
                        </a>
                        <a
                            href="<?php echo $CFG->wwwroot . '/login/forgot_password.php'; ?>"
                            class="pwd-forgot-link">
                            <?php print_string("forgotten") ?>
                        </a>
                    </div>
                </div>
            </section>
            <?php 
                echo $OUTPUT->main_content();
            ?>
            <section class="is--cta">
                <div class="cta-container">
                    <a  
                        href="<?php echo $CFG->wwwroot . '/login/signup.php'; ?>"
                        class="btn btn-primary btn-block btn-large">
                        Join Mokolo
                    </a>
                </div>
            </section>

            <?php
            require_once(dirname(__FILE__) . '/includes/footer.php');
            ?>

        </div>
    </div>
    
<?php
require_once(dirname(__FILE__) . '/includes/end_of_html.php');
?>