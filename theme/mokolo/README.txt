=============================
For the sake of simplicity...
=============================

... I already committed the pre-built files into the ./dist folder. I would not do this normally, but this makes it easier for you I guess. However, below starts the way how to build them on your own!

==========================
How to setup build process
==========================

# install NPM
$> Go to https://nodejs.org/ and follow instructions to install NodeJS and NPM

# install Grunt
$> npm install -g grunt-cli

# install Dependencies
$> npm install

================================
How to build the necessary files
================================

# build once
$> grunt default --env=dev

# build continuously when files changed
$> grunt default simple-watch --env=dev

====================================
How to open the files in the browser
====================================

# start a simple web browser with python
$> python -m SimpleHTTPServer 8080

# open browser and go to
http://localhost:8080