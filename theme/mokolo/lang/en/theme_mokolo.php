<?php

$string['pluginname'] = 'Mokolo';

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Theme: Mokolo</h2>
</div>
</div>';

$string['subjects'] = 'Subjects';
$string['about'] = 'About';
$string['home'] = 'Home';
$string['myprogress'] = 'My Progress';

$string['backtocourses'] = 'Back to courses';