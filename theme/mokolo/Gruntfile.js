module.exports = function(grunt) {

    var config = {
        sourceDir: './',
        targetDirCss: './style',
        targetDirJs: './javascript',
        environment: grunt.option('env') || 'prod'
    };

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            'target': [ config.targetDirCss, config.targetDirJs ],
            'css': [ config.targetDirCss ],
            'js': [ config.targetDirJs ]
        },

        concat: {
            'third': {
                nonull: true,
                src: [
                    config.sourceDir + 'js/third/*.js'
                ],
                dest: config.targetDirJs + '/libs.js'
            },
            'own': {
                src: [
                    //app
                    config.sourceDir + '/js/*.js',
                ],
                dest: config.targetDirJs + '/script.js'
            }
        },

        uglify: {
            'third': {
                files: [{
                    dest: config.targetDirJs + '/libs.js',
                    src: config.targetDirJs + '/libs.js'
                }]
            },
            'own': {
                files: [{
                    dest: config.targetDirJs + '/script.js',
                    src: config.targetDirJs + '/script.js'
                }]
            }
        },

        less: {
            development: {
                options: {
                    compress: false
                },
                files: [{
                    dest: config.targetDirCss + '/mokolo.css',
                    src: config.sourceDir + '/less/mokolo.less'
                }]
            },
            production: {
                options: {
                    compress: true
                },
                files: [{
                    dest: config.targetDirCss + '/mokolo.css',
                    src: config.sourceDir + '/less/mokolo.less'
                }]
            }
        },

        watch: {
            less: {
                files: [
                    config.sourceDir + '/less/**/*.less'
                ],
                tasks: [
                    'clean:css',
                    'less:development'
                ],
                options: {
                }
            },
            js: {
                files: [
                    config.sourceDir + '/js/**/*.js'
                ],
                tasks: [
                    'clean:js',
                    'concat:third',
                    'concat:own'
                ],
                options: {
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');

    if (config.environment === 'dev') {
        grunt.loadNpmTasks('grunt-simple-watch');
        grunt.loadNpmTasks('grunt-contrib-watch');
    }

    if (config.environment !== 'dev') {
        // PRODUCTION task(s).
        grunt.registerTask('default', [
            'clean:target',
            'concat:third',
            'concat:own',
            'uglify:third',
            'uglify:own',
            'less:production'
        ]);
    } else {
        // DEVELOPMENT task(s).
        grunt.registerTask('default', [
            'clean:target',
            'concat:third',
            'concat:own',
            'less:development'
        ]);

    }
}
