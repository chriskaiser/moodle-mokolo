=============================
Everything we need to know to setup moodle
=============================

For a local installation, use:
https://github.com/digitalsparky/moodle-vagrant

We stick with Moodle v3.0.3

===============================
How to install the Mokolo Theme / Plugin
===============================

https://docs.moodle.org/30/en/Installing_plugins

ln -s /.../main-repository/moodle/theme/mokolo /.../path/to/moodle/theme/mokolo

Site Administration -> Notifications -> Continue Install
Site Administration -> Appearance -> Themes -> Theme Settings -> "Theme designer mode -> yes"
Site Administration -> Appearance -> Themes -> Theme selector -> "Change Theme" -> "Mokolo"
