<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "progress" - View page
 *
 * @package    local_progress
 * @copyright  2016 mokolo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
// defined('MOODLE_INTERNAL') || die(); - Must not be called because this script is called from outside moodle
// Include lib.php
require_once(dirname(__FILE__) . '/lib.php');

// Include config.php
require_once('../../config.php');

require_once('../../lib/coursecatlib.php');

// Require login, otherwise this page doesnt make any sense
require_login();

// Globals
global $PAGE;

// Get plugin config
$local_progress_config = get_config('local_progress');

// set all necessary $PAGE parameters
$PAGE->set_url('/local/progress/view.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('myprogress');
$PAGE->set_title($local_progress_config->progresspagetitle);   // probably make this a setting
$PAGE->set_heading($local_progress_config->progresspagetitle);
$PAGE->navbar->add($local_progress_config->progresspagetitle);

// page header
echo $OUTPUT->header();


$out = '';
// using following structure:
// site -> category -> subcategory (beginner, advanced, further) -> course -> section
// get categories
$cats = coursecat::get(0)->get_children();

foreach ($cats as $cat) {
    // get subcategories
    $subcats = $cat->get_children();

    if (count($subcats) == 0) {
        continue;
    }

    $out .= html_writer::start_div('clearfix progress-category progress-category--' . count($subcats));
    $out .= html_writer::tag('h2', $cat->name, array('class' => 'progress-category_title'));

    foreach ($subcats as $subcat) {

        // DATA-COLLECTION
        $coursesarray = array();
        $coursesarray['courses'] = array();
        $coursesarray['numsectionsoverall'] = 0;
        $coursesarray['numsectionscomplete'] = 0;

        // get courses
        $courses = $subcat->get_courses();
        foreach ($courses as $course) {
            
            $coursearray = array();

            $modinfo = get_fast_modinfo($course->id);
            $completion = new completion_info($modinfo->get_course());

            $urltocourse = new moodle_url('/course/view.php',
                array('id' => $course->id));

            $coursearray['url'] = $urltocourse;
            $coursearray['name'] = $course->shortname;
            $coursearray['sections'] = array();

            foreach ($modinfo->get_sections() as $section => $cms) {
                if ($section == 0) {
                    continue;
                }

                $thissection = $modinfo->get_section_info($section);

                $showsection = 
                    $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));

                if (!$showsection) {
                    if (!$course->hiddensections && $thissection->available) {
                        echo $this->section_hidden($section, $course->id);
                    }

                    continue;
                }

                $sectionarray = array();

                $urltosection = new moodle_url('/course/view.php',
                    array('id' => $course->id,
                          'section' => $section));

                $sectionarray['url'] = $urltosection;

                // MOKOLO_COMPLETION
                // In default Moodle, a SECTION/TOPIC has NOT the ability to be complete
                // hence, we define here, that whenever ONE of the activities/resources
                // inside the SECTION/TOPIC is complete, then the whole thing is complete
                $section_is_complete = false;

                foreach ($cms as $modnumber) {
                    $cm = $modinfo->cms[$modnumber];
                    $completion_cm = $completion->get_data($cm);

                    if ($completion_cm->completionstate != COMPLETION_INCOMPLETE) {
                        $section_is_complete = true;
                    }
                }

                $sectionarray['iscomplete'] = $section_is_complete;

                $coursearray['sections'][] = $sectionarray;
                $coursesarray['numsectionsoverall']++;
                if ($section_is_complete) {
                    $coursesarray['numsectionscomplete']++;
                }
            }

            $coursesarray['courses'][] = $coursearray;
        }


        // RENDERING
        $out .= html_writer::start_div('progress-difficulty progress-colorscheme progress-colorscheme--' . strtolower($subcat->name));

        $overallcompletionrate = $coursesarray['numsectionsoverall'] == 0 ? 0 : $coursesarray['numsectionscomplete'] * 100 / $coursesarray['numsectionsoverall'];
        $overallcompletionrate = round($overallcompletionrate, 2);

        $out .= html_writer::start_div('progress-overall-bar');

        $out .= html_writer::start_span('progress-overall-bar-completion', array('style' => 'width:' . $overallcompletionrate . '%;'));
        $out .= html_writer::end_span();

        $out .= html_writer::end_div(); // progress-overall-bar

        foreach ($coursesarray['courses'] as $coursearray) {

            $out .= html_writer::start_tag('a', array('href' => $coursearray['url'], 'class' => 'clearfix progress-course'));
            $out .= html_writer::tag('span', $coursearray['name'], array('class' => 'progress-course_title'));

            $out .= html_writer::start_div('progress-course_completion-wrapper');

            $out .= html_writer::start_tag('ul', array('class' => 'dotted_progress'));

            foreach ($coursearray['sections'] as $sectionarray) {

                if ($sectionarray['iscomplete']) {
                    $out .= html_writer::tag('li', '',
                        array('class' => 'dotted_progress-item dotted_progress-item--is-achieved'));
                } else {
                    $out .= html_writer::tag('li', '',
                        array('class' => 'dotted_progress-item'));
                }
            }

            $out .= html_writer::end_tag('ul'); // dotted_progress

            $out .= html_writer::end_div();  // progress-course_completion-wrapper
            $out .= html_writer::end_tag('a'); // /course
        }

        $out .= html_writer::end_div();     // /subcategory
    }

    $out .= html_writer::end_div(); // /category
}

echo $out;    
echo $OUTPUT->footer();
