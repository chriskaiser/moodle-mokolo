<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "progress" - Settings
 *
 * @package    local_progress
 * @copyright  2016 mokolo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// Include lib.php
require_once(dirname(__FILE__) . '/lib.php');

global $CFG, $PAGE;

if ($hassiteconfig) {
    // New settings page
    $page = new admin_settingpage('progress', get_string('pluginname', 'local_progress'));

    // Page heading
    $page->add(new admin_setting_heading('local_progress/progresspagetitleheading', get_string('progresspagetitle', 'local_progress'), ''));
    
    // Page heading text box widget
    $page->add(new admin_setting_configtext('local_progress/progresspagetitle', get_string('progresspagetitle', 'local_progress'), get_string('progresspagetitle_desc', 'local_progress'), get_string('progresspagetitle_default', 'local_progress')));
    
    // Add settings page to navigation tree
    $ADMIN->add('localplugins', $page);
}
