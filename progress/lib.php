<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local plugin "progress" - Library
 *
 * @package    local_progress
 * @copyright  2016 mokolo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// defined('MOODLE_INTERNAL') || die(); // - Must not be called because this script is called from outside moodle

/**
 * This extends the site navigation so a link to the progress page is added.
 * @param global_navigation $navigation
 */
function local_progress_extend_navigation(global_navigation $navigation) {
    $local_progress_config = get_config('local_progress');
    
    if ((isset($local_progress_config->progresspagetitle) || property_exists($local_progress_config, 'progresspagetitle')) && !empty($local_progress_config->progresspagetitle)) { 
        $title = $local_progress_config->progresspagetitle;
    } else {
        $title = get_string('local_progress', 'progresspagetitle_default');
    }
    $navigation->add($title, new moodle_url('/local/progress/view.php'));
}