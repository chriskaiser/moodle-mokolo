# Mokolo Progress Plugin

## Installation

### Manuell

Das Video Plugin kann installiert werden, indem der Ordner *progress* mitsamt Inhalt in den Ordner *moodle/local* kopiert wird.

- Den Ordner **progress** mitsamt Inhalt in den Ordner **moodle/local** der Moodle-Installation kopieren
- Innerhalb des Admin-Bereichs von Moodle die dann erscheinende Aufforderung zur Installation des Plugins bestätigen
- Falls die Aufforderung zur Installation nicht automatisch erscheint, zu **Site administration -> Notifications** navigieren und dort die Installation bestätigen
 
### Zip-Upload

- Als Admin anmelden
- Zu **Site administration -> Plugins -> Install plugins** navigieren
- **progress.zip** hochladen und **Install plugin from the ZIP file** klicken

## Konfiguration

Bislang einzige Einstellmöglichkeit ist der Name der Seite. Dieser taucht sowohl als Überschrift auf der Seite selbst als auch im Hauptmenü als Name des Links zum Plugin auf.

## Benutzung

Das Plugin fügt automatisch einen Link im Hauptmenü hinzu.