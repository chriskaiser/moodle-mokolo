<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

$id = required_param('id', PARAM_INT);      // course id

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);

require_course_login($course, true);
$PAGE->set_pagelayout('incourse');

$params = array(
    'context' => context_course::instance($course->id)
);
$event = \mod_video\event\course_module_instance_list_viewed::create($params);
$event->add_record_snapshot('course', $course);
$event->trigger();

$strvideo = get_string('modulename', 'video');
$strvideos = get_string('modulenameplural', 'video');
$strname = get_string('name');
$strintro = get_string('moduleintro');
$strlastmodified = get_string('lastmodified');

$PAGE->set_url('/mod/url/index.php', array('id' => $course->id));
$PAGE->set_ittle($course->shortname.':'.$strvideos);
$PAGE->set_heading($course->fullname);
$PAGE->navbar->add($strvideos);
echo $OUTPUT->header();
echo $OUTPUT->heading($strvideos);

if (!$videos = get_all_instances_in_course('video', $course)) {
    notice(get_string('thereareno', 'moodle', $strvideos), "$CFG->wwwroot/course/view.php?id=$course->id");
    exit;
}

$usesections = course_format_uses_sections($course->format);

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($usesections) {
    $strsectionname = get_string('sectionname', 'format_'.$course->format);
    $table->head = array($strsectionname, $strname, $strintro);
    $table->align('left', 'left', 'left');
} else {
    $table->head = array($strlastmodified, $strname, $strintro);
    $table->align = array('left', 'left', 'left');
}

$modinfo = get_fast_modinfo($course);
$currentsection = '';

foreach ($videos as $video) {
    $cm = $modinfo->cms[$video->coursemodule];
    if ($usesections) {
        $printsection = '';
        if ($video->section !== $currentsection) {
            if ($video->section) {
                $printsection = get_section_name($course, $video->section);
            }
            if ($currentsection !== '') {
                $table->data[] = 'hr';
            }
            $currentsection = $video->section;
        }
    } else {
        $printsection = '<span class="smallinfo">'.userdate($video->timemodified).'</span>';
    }
    
    $extra = empty($cm->extra) ? '' : $cm->extra;
    $icon = '';
    if (!empty($cm->icon)) {
        $icon = '<img src="'.$OUTPUT->pix_url($cm->icon).' class="activityicon" alt="'.get_string('modulename', $cm->modname).'" />';
    }
    
    $class = $video->visible ? '' : 'class="dimmed"';
    
    $table->data[] = array(
        $printsection,
        '<a $class $extra href="view.php?id='.$cm->id.'">'.$icon.format_string($video->name).'</a>',
        format_module_intro('video', $video, $cm->id),
    );
}
echo html_writer::table($table);

echo $OUTPUT->footer();