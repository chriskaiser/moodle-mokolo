<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

function video_supports($feature) {
    // commented cases are already at default value apparently
    switch ($feature) {
        // video is a resource
        case FEATURE_MOD_ARCHETYPE:             return MOD_ARCHETYPE_RESOURCE;
        // video has no grading, is a resource, not an activity
        case FEATURE_GRADE_HAS_GRADE:           return false;
        case FEATURE_GRADE_OUTCOMES:            return false;
        //case FEATURE_ADVANCED_GRADING:          return false;
        //case FEATURE_CONTROLS_GRADE_VISIBILITY: return false;
        //case FEATURE_PLAGIARISM:                return false;
            
        // viewing the activity completes it, should do the job for now
        case FEATURE_COMPLETION_TRACKS_VIEWS:   return true;
            
        // TODO: consider FEATURE_COMPLETION_HAS_RULES
        // check if user actually watched the video? how?
        case FEATURE_COMPLETION_HAS_RULES:      return false;
            
        //case FEATURE_NO_VIEW_LINK:              return false;
        //case FEATURE_IDNUMBER:                  return false;
        case FEATURE_GROUPS:                    return false;
        case FEATURE_GROUPINGS:                 return false;
            
        // TODO: not so sure about MOD_INTRO, do we need it?
        case FEATURE_MOD_INTRO:                 return true;
            
        // TODO: "true if module has default completion"?
        //case FEATURE_MODEDIT_DEFAULT_COMPLETION:return true;

        // comments and rating enabled
        case FEATURE_COMMENT:                   return true;
        case FEATURE_RATE:                      return true;
            
        // we are working with moodle 3
        // case FEATURE_BACKUP_MOODLE2:            return false;
        // FEATURE_SHOW_DESCRIPTION: module shows description on course page
        case FEATURE_SHOW_DESCRIPTION:          return true;
            
        default:
            return null;
    }
}

/**
 * Add video instance
 * @global type $DB
 * @param type $data
 * @param type $mform
 * @return type
 */
function video_add_instance($data, $mform = null) {
    global $DB;
    
    $data->timemodified = time();
    $data->id = $DB->insert_record('video', $data);
    
    return $data->id;
}

/**
 * Update video instance
 * @param type $data
 * @param type $mform
 * @return boolean
 */
function video_update_instance($data, $mform) {
    global $DB;
    
    $data->timemodified = time();
    $data->id = $data->instance;
    
    $DB->update_record('url', $data);
    
    return true;
}

/**
 * Delete video instance
 * @global type $DB
 * @param type $id
 * @return boolean
 */
function video_delete_instance($id) {
    global $DB;
    
    if (!$video = $DB->get_record('video', array('id'=>$id))) {
        return false;
    }
    
    $DB->delete_records('video', array('id'=>$video->id));
    
    return true;
}

/**
 * Mark the video viewed and trigger course_module_viewed event
 * @param type $video
 * @param type $course
 * @param type $cm
 * @param type $context
 */
function video_view($video, $course, $cm, $context) {
    $params = array(
        'context' => $context,
        'objectid' => $video->id,
    );
    
    $event = \mod_video\event\course_module_viewed::create($params);
    $event->add_record_snapshot('course_modules', $cm);
    $event->add_record_snapshot('course', $course);
    $event->add_record_snapshot('video', $video);
    $event->trigger();
    
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}