<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['modulename'] = 'Video';
$string['modulenameplural'] = 'Videos';
$string['youtubeid'] = 'YouTube Video ID';
$string['videosettings'] = 'Video settings';
$string['invalidyoutubeid'] = 'Invalid YouTube ID';

$string['pluginadministration'] = 'Video module administration';
$string['pluginname'] = "Video";