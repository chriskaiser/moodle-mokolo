<?php

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/video/locallib.php');

class mod_video_mod_form extends moodleform_mod {
    function definition() {
        global $CFG, $DB, $OUTPÚT;
        
        $mform = $this->_form;
        
        //---- general ----
        $mform->addElement('header', 'general', get_string('general', 'form'));
        $mform->addElement('text', 'name', get_string('name'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $this->standard_intro_elements();
        
        // ---- video settings ----
        $mform->addElement('header', 'video', get_string('videosettings', 'video'));
        $mform->addElement('text', 'youtubeid', get_string('youtubeid', 'video'), array('size'=>'11'));
        $mform->setType('youtubeid', PARAM_RAW);    // we validate this in validation()
        $mform->addRule('youtubeid', null, 'required', null, 'client');
        
        // ----
        $this->standard_coursemodule_elements();
        
        // ----
        $this->add_action_buttons();
    }
    
    function data_preprocessing(&$default_values) {
        parent::data_preprocessing($default_values);
    }
    
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        
        if (!empty($data['youtubeid'])) {
            if (!video_youtubeid_is_valid($data['youtubeid'])) {
                $errors['youtubeid'] = get_string('invalidyoutubeid', 'video');
            }
        }
        
        return $errors;
    }
}