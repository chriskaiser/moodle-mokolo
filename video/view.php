<?php

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once("$CFG->dirroot/mod/video/lib.php");
require_once("$CFG->dirroot/mod/video/locallib.php");

$id = optional_param('id', 0, PARAM_INT);
$v = optional_param('v', 0, PARAM_INT);

if ($v) {
    $video = $DB->get_record('video', array('id'=>$v), '*' , MUST_EXIST);
    $cm = get_coursemodule_from_instance('video', $video->id, $video->course, false, MUST_EXIST);
} else {
    $cm = get_coursemodule_from_id('video', $id, 0, false, MUST_EXIST);
    $video = $DB->get_record('video', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/video:view', $context);

video_view($video, $course, $cm, $context);

$PAGE->set_url('/mod/video/view.php', array('id' => $cm->id));

$ytid = trim($video->youtubeid);
if (empty($ytid) or $ytid === '') {
    // TODO
    // video_print_header
    // video_print_heading
    // video_print_intro
    // notice invalidyoutubeid
    die;
}
unset($ytid);

// TODO: multiple video sources (internal, youtube, ...)
// $videotype = video_get_video_type($video);
// switch ($videotype)

video_display_youtube_video($video, $cm, $course);
