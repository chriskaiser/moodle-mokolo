<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'mod/video:view' => array(
        'captype'       => 'read',
        'contextlevel'  => CONTEXT_MODULE,
        'archetypes'    => array(
            'guest'         => CAP_ALLOW,
            'user'          => CAP_ALLOW,
        ),
    ),
    
    'mod/video:addinstance' => array(
        'riskbitmask'   => RISK_SPAM | RISK_XSS,
        'captype'       => 'write',
        'contextlevel'  => CONTEXT_COURSE,
        'archetypes'    => array(
            'editingteacher'    => CAP_ALLOW,
            'manager'           => CAP_ALLOW,
        ),
    ),
);