# Video Plugin

## Installation

### Manuell

Das Video Plugin kann installiert werden, indem der Ordner *video* mitsamt Inhalt in den Ordner *moodle/mod* kopiert wird.

- Den Ordner **video** mitsamt Inhalt in den Ordner **moodle/mod** der Moodle-Installation kopieren.
- Innerhalb des Admin-Bereichs von Moodle die dann erscheinende Aufforderung zur Installation des Plugins bestätigen.
 
### Zip-Upload

- Als Admin anmelden
- Zu **Site administration -> Plugins -> Install plugins** navigieren
- **mod_video.zip** hochladen und **Install plugin from the ZIP file** klicken

## Benutzung
Das Plugin taucht nach der Installation als Resource auf, die wie gewohnt zu bestehenden Kursen hinzugefügt werden kann. Beim Erstellen wird eine YouTube-ID benötigt, die sich aus der URL des Videos ablesen lässt: Aus [https://www.youtube.com/watch?v=***DLzxrzFCyOs***](https://www.youtube.com/watch?v=DLzxrzFCyOs) wird ***DLzxrzFCyOs***.
