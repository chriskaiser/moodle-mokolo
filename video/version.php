<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component  = 'mod_video';
$plugin->version    = 2016032000;
$plugin->requires   = 2015111602.10;
$plugin->maturity   = MATURITY_ALPHA;
