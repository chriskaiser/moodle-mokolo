<?php

// This file is part of the video module for moodle

/**
 * @package     mod_video
 * @copyright   2016, Simon Pähler <whitecorps@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;


/**
 * Checks a string 
 * @param type $ytid
 * @return type
 */
function video_youtubeid_is_valid($ytid) {
    // regex check for youtube id (11 chars, alphanumeric)
    return (bool)preg_match('/^([a-zA-z0-9]{11})/i', $ytid);
}

function video_url_to_youtubeid($url) {
    // TODO: create regex that extracts the video id from the url
    // need to consider multiple url formats
    // youtu.be/<id>
    // youtube.com/watch?v=<id>&etc
    // etc
    return null;
}

function video_print_header($video, $cm, $course) {
    global $PAGE, $OUTPUT;
    
    $PAGE->set_title($course->shortname.': '.$video->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($video);
    echo $OUTPUT->header();
}

function video_print_heading($video, $cm, $course, $notused = false) {
    global $OUTPUT;
    echo $OUTPUT->heading(format_string($video->name), 2);
}

function video_print_intro($video, $cm, $course, $ignoresettings=false) {
    global $OUTPUT;
    
    if (!empty($options['printintro'])) {
        if (trim(strip_tags($video->intro))) {
            echo $OUTPUT->box_start('mod_introbox', 'videointro');
            echo format_module_intro('video', $video, $cm->id);
            echo $OUTPUT->box->end();
        }
    }
}

function video_display_youtube_video($video, $cm, $course) {
    global $OUTPUT;

    // TODO
    // video_print_header
    video_print_header($video, $cm, $course);
    // video_print_heading
    video_print_heading($video, $cm, $course);
    // video_print_youtube_embed_code
    // TODO: uploaded video or other links: see outputrenderers.php: core_media_renderer->embed_url
    // TODO: use youtube embed code
    //echo '<div><p>Hello video world: '.$video->youtubeid.'</p></div>';
    
    echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$video->youtubeid.'" frameborder="0" allowfullscreen></iframe>';
    
    // video_print_intro
    video_print_intro($video, $cm, $course);
    
    
    echo $OUTPUT->footer();
    die();
}